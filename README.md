# Setup
1. install frontend - dashboard
2. install backend
3. put `set DEBUG=backend:* &` in `package.json` in start script
4. install backend node_modules using yarn
5. change port using `set PORT=2000`
6. install new package `yarn add cross-env --dev`
7. install new package `yarn add nodemon --dev`
8. change the way you are setting the env var
9. start should be like this `cross-env PORT=2000 DEBUG=backend:* nodemon node ./bin/www`
10. install `cross-env` in frontend
11. change start script to `cross-env PORT=4000 react-scripts start`
12. `yarn init` in the root folder
13. `yarn add concurrently -D`
14. ignore projects in the `.gitignore` file inside the root folder
15. push everything to a group of projects on gitlab with "Initial commit" as message

# Connect to Mongo, Modeling, Controller and Routes

## Install packages
1. `cd .\backend\`
2. `yarn add dotenv mongoose`

## Connect to Mongo
1. add `.env` file and put env variables
2. add `.env` to `.gitignore` file to ignore it
3. import `dotenv` and `mongoose` inside `app.js`
4. connect to db

## Model DB
### Models: category, author, book
1. see models
2. see controllers
3. see routes

## Image Uploader
1. `yarn add multer` to install the file uploader
2. create upload folder inside the public folder
3. add upload to .gitignore file to ignore it
4. add File model, controller and routes